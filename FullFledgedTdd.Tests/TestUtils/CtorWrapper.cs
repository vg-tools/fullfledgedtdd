using System;
using System.Collections.Generic;
using Autofac;
using Autofac.Core;

namespace FullFledgedTdd.Tests.TestUtils
{
    public class CtorWrapper<T>
    {
        private readonly List<IEnumerable<Parameter>> calls = new List<IEnumerable<Parameter>>();

        public CtorWrapper(T instance, int count = -1)
        {
            Factory = (c, ps) =>
                          {
                              if (count != -1 && calls.Count == count)
                              {
                                  throw new Exception(string.Format("Factory is called more than {0} times.", count));
                              }
                              calls.Add(ps);
                              return instance;
                          };
        }

        public CtorWrapper(T[] instances)
        {
            if (instances == null)
            {
                throw new ArgumentNullException("instances");
            }

            if (instances.Length == 0)
            {
                throw new ArgumentException("instances");
            }

            Factory = (c, ps) =>
                          {
                              if (calls.Count == instances.Length)
                              {
                                  throw new Exception(string.Format("Factory is called to more times (max = {0}).", instances.Length));
                              }
                              calls.Add(ps);
                              return instances[calls.Count - 1];
                          };
        }

        public Func<IComponentContext, IEnumerable<Parameter>, T> Factory { get; private set; }

        public IEnumerable<IEnumerable<Parameter>> Calls
        {
            get { return calls; }
        }
    }
}