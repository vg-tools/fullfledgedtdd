using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.TestUtils
{
    public class ActionTestHelper
    {
        readonly List<Guid> calls = new List<Guid>();

        public ActionRecord<T> CreateAction<T>()
        {
            var id = Guid.NewGuid();
            return new ActionRecord<T>(id, c => calls.Add(id));
        }

        public ActionRecord CreateAction()
        {
            var id = Guid.NewGuid();
            return new ActionRecord(id, () => calls.Add(id));
        }

        public void VerifyAction(ActionRecord actionRecord, Action action)
        {
            calls.Clear();

            Assert.AreEqual(0, calls.Count(c => c == actionRecord.Id));
            action();
            Assert.AreEqual(1, calls.Count(c => c == actionRecord.Id));
            action();
            Assert.AreEqual(2, calls.Count(c => c == actionRecord.Id));
        }

        public void VerifyAction<T>(ActionRecord<T> actionRecord, Action<T> action)
        {
            calls.Clear();

            var res =
                action != null &&
                calls.Count(c => c == actionRecord.Id) == 0;

            if (res)
            {
                action(default(T));
                res = calls.Count(c => c == actionRecord.Id) == 1;
                if (res)
                {
                    action(default(T));
                    res = calls.Count(c => c == actionRecord.Id) == 2;
                }
            }

            if (!res)
            {
                Assert.Fail("Actions are different.");
            }
        }

        public class ActionRecord<T>
        {
            public ActionRecord(Guid id, Action<T> action)
            {
                Id = id;
                Action = action;
            }

            public Guid Id { get; private set; }
            public Action<T> Action { get; private set; }
        }

        public class ActionRecord
        {
            public ActionRecord(Guid id, Action action)
            {
                Id = id;
                Action = action;
            }

            public Guid Id { get; private set; }
            public Action Action { get; private set; }
        }
    }
}