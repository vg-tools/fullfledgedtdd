using System;
using Autofac;

namespace FullFledgedTdd.Tests.TestUtils
{
    public class ContainerHelper
    {
        private readonly ContainerBuilder builder;
        private readonly IContainer container;

        public ContainerHelper()
        {
            builder = new ContainerBuilder();
            container = builder.Build();
        }

        public IContainer Container
        {
            get { return container; }
        }

        public void Setup(Action<ContainerBuilder> setupDelegate)
        {
            var b = new ContainerBuilder();
            setupDelegate(b);
            b.Update(container);
        }
    }
}