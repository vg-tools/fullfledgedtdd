using System.Collections.Generic;
using Autofac;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.Tests.TestHelpers;
using FullFledgedTdd.Tests.TestUtils;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace FullFledgedTdd.Tests.ActionDefinition
{
    [TestFixture]
    public class ActionSetOperatorsTests
    {
        #region Setup

        private ContainerHelper containerHelper;

        private ActionSet<DummyTestContext> step1;
        private ActionSet<DummyTestContext> step2;

        private ICombinationActionSet<DummyTestContext> combinationActionSet;
        private IUnionActionSet<DummyTestContext> unionActionSet;

        private CtorWrapper<ICombinationActionSet<DummyTestContext>> combinationCtor;
        private CtorWrapper<IUnionActionSet<DummyTestContext>> unionCtor;

        [SetUp]
        public void SetUp()
        {
            step1 = Mock.Of<ActionSet<DummyTestContext>>();
            step2 = Mock.Of<ActionSet<DummyTestContext>>();

            combinationActionSet = Mock.Of<ICombinationActionSet<DummyTestContext>>();
            unionActionSet = Mock.Of<IUnionActionSet<DummyTestContext>>();

            //container setup
            containerHelper = new ContainerHelper();
            FullFledgedTddModule.ComponentContext = containerHelper.Container;
        } 

        [TearDown]
        public void TearDown()
        {
            FullFledgedTddModule.ComponentContext = null;
        }

        #endregion

        [Test]
        public void OperatorAnd()
        {
            combinationCtor = new CtorWrapper<ICombinationActionSet<DummyTestContext>>(combinationActionSet, 1);
            containerHelper.Setup(b => b.Register<ICombinationActionSet<DummyTestContext>>(combinationCtor.Factory));
            

            var res = step1 & step2;


            Assert.AreSame(combinationActionSet, res);

            var call = combinationCtor.Calls.Single();
            CollectionAssert.AreEqual(new[] { step1, step2 }, call.Named<IEnumerable<ActionSet<DummyTestContext>>>("actionSets"));
        }

        [Test]
        public void OperatorOr()
        {
            unionCtor = new CtorWrapper<IUnionActionSet<DummyTestContext>>(unionActionSet, 1);
            containerHelper.Setup(b => b.Register<IUnionActionSet<DummyTestContext>>(unionCtor.Factory));


            var res = step1 | step2;


            Assert.AreSame(unionActionSet, res);

            var call = unionCtor.Calls.Single();
            CollectionAssert.AreEqual(new[] { step1, step2 }, call.Named<IEnumerable<ActionSet<DummyTestContext>>>("actionSets"));
        }
    }
}