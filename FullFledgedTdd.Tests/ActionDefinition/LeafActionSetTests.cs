using System;
using Autofac;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.TestDefinition;
using FullFledgedTdd.Tests.TestHelpers;
using FullFledgedTdd.Tests.TestUtils;
using NUnit.Framework;
using System.Linq;

namespace FullFledgedTdd.Tests.ActionDefinition
{
    [TestFixture]
    public class LeafActionSetTests
    {
        #region Setup

        private DummyRunAndDummyActionSetHelper c;
        private ContainerHelper containerHelper;
        private ActionTestHelper actionTestHelper;

        private ActionTestHelper.ActionRecord<DummyTestContext> actionRecord;
        private ActionTestHelper.ActionRecord<DummyTestContext> tearDownActionRecord;
        private CtorWrapper<IRun<DummyTestContext>> runCtor;

        [SetUp]
        public void SetUp()
        {
            c = new DummyRunAndDummyActionSetHelper();
            actionTestHelper = new ActionTestHelper();

            actionRecord = actionTestHelper.CreateAction<DummyTestContext>();
            tearDownActionRecord = actionTestHelper.CreateAction<DummyTestContext>();

            //container setup
            containerHelper = new ContainerHelper();

            runCtor = new CtorWrapper<IRun<DummyTestContext>>(new[] { c.run1_1, c.run1_2 });

            containerHelper.Setup(b => b.Register<IRun<DummyTestContext>>(runCtor.Factory));
            containerHelper.Setup(b => b.RegisterType<LeafActionSet<DummyTestContext>>().As<ILeafActionSet<DummyTestContext>>());
            containerHelper.Setup(b => b.RegisterGeneric(typeof(LeafActionSet<>)).As(typeof(ILeafActionSet<>)));
        }

        [TearDown]
        public void TearDown()
        {
            FullFledgedTddModule.ComponentContext = null;
        }

        #endregion

        [Test]
        public void OneAction_GetRunSequencesTest()
        {
            var factory = containerHelper.Container.Resolve<LeafActionSetFactory<DummyTestContext>>();
            var target = factory(actionRecord.Action);


            var runs1 = target.GetRuns();
            CollectionAssert.AreEqual(
                new[]
                    {
                        new []{c.run1_1},
                    },
                runs1
            );

            var runs2 = target.GetRuns();
            CollectionAssert.AreEqual(
                new[]
                    {
                        new []{c.run1_2},
                    },
                runs2
            );

            Assert.AreEqual(2, runCtor.Calls.Count());

            foreach (var call in runCtor.Calls)
            {
                Assert.AreEqual(3, call.Count());
                actionTestHelper.VerifyAction(actionRecord, call.Named<Action<DummyTestContext>>("action"));
                Assert.IsNull(call.Named<Action<DummyTestContext>>("tearDownAction"));
                Assert.IsFalse(call.Named<bool>("isValidation"));
            }
        }

        [Test]
        public void ActionAndTearDownAction_GetRunSequencesTest()
        {
            var factory = containerHelper.Container.Resolve<LeafActionSetFactoryWithTearDown<DummyTestContext>>();
            var target = factory(
                actionRecord.Action,
                tearDownActionRecord.Action
            );


            var runs1 = target.GetRuns();
            CollectionAssert.AreEqual(
                new[]
                    {
                        new []{c.run1_1},
                    },
                runs1
            );

            //Test second call of GetRuns()
            var runs2 = target.GetRuns();
            CollectionAssert.AreEqual(
                new[]
                    {
                        new []{c.run1_2},
                    },
                runs2
            );

            Assert.AreEqual(2, runCtor.Calls.Count());

            foreach (var call in runCtor.Calls)
            {
                Assert.AreEqual(3, call.Count());
                actionTestHelper.VerifyAction(actionRecord, call.Named<Action<DummyTestContext>>("action"));
                actionTestHelper.VerifyAction(tearDownActionRecord, call.Named<Action<DummyTestContext>>("tearDownAction"));
                Assert.IsFalse(call.Named<bool>("isValidation"));
            }
        }
    }
}