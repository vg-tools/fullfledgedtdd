using Autofac;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.Tests.TestHelpers;
using FullFledgedTdd.Tests.TestUtils;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.ActionDefinition
{
    [TestFixture]
    public class CombinationActionSetTests
    {
        #region Setup

        private ContainerHelper containerHelper;
        private DummyRunAndDummyActionSetHelper c;

        [SetUp]
        public void SetUp()
        {
            c = new DummyRunAndDummyActionSetHelper();

            //container setup
            containerHelper = new ContainerHelper();
            containerHelper.Setup(b => b.RegisterGeneric(typeof(CombinationActionSet<>)).As(typeof(ICombinationActionSet<>)));
        }

        [TearDown]
        public void TearDown()
        {
            FullFledgedTddModule.ComponentContext = null;
        }

        #endregion

        [Test]
        public void GetRunSequencesTest()
        {
            var factory = containerHelper.Container.Resolve<CombinationActionSetFactory<DummyTestContext>>();
            var target = factory(new[] { c.actionSet1, c.actionSet2 });


            CollectionAssert.AreEqual(
                new[] {
                    new[] {c.run1_1,c.run3_1},
                    new[] {c.run1_1,c.run4_1},
                    new[] {c.run2_1,c.run3_1},
                    new[] {c.run2_1,c.run4_1},
                },
                 target.GetRuns()
            );

            CollectionAssert.AreEqual(
                new[] {
                    new[] {c.run1_2,c.run3_2},
                    new[] {c.run1_2,c.run4_2},
                    new[] {c.run2_2,c.run3_2},
                    new[] {c.run2_2,c.run4_2},
                },
                 target.GetRuns()
            );
        }
    }
}
