using System;
using Autofac;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.TestDefinition;
using FullFledgedTdd.Tests.TestHelpers;
using FullFledgedTdd.Tests.TestUtils;
using NUnit.Framework;
using System.Linq;

namespace FullFledgedTdd.Tests.ActionDefinition
{
    [TestFixture]
    public class ValidateActionSetTests
    {
        #region Setup

        private DummyRunAndDummyActionSetHelper c;
        private ContainerHelper containerHelper;
        private ActionTestHelper actionTestHelper;

        private ActionTestHelper.ActionRecord<DummyTestContext> actionRecord;
        /*private ActionTestHelper.ActionRecord<DummyTestContext> tearDownActionRecord;*/

        private DummyRun validationRun1;
        private DummyRun validationRun2;
        private CtorWrapper<IRun<DummyTestContext>> validationRunCtor;

        [SetUp]
        public void SetUp()
        {
            c = new DummyRunAndDummyActionSetHelper();
            actionTestHelper = new ActionTestHelper();

            actionRecord = actionTestHelper.CreateAction<DummyTestContext>();
            /*tearDownActionRecord = actionTestHelper.CreateAction<DummyTestContext>();*/

            validationRun1 = new DummyRun { Key = "v_1" };
            validationRun2 = new DummyRun { Key = "v_2" };

            //container setup
            containerHelper = new ContainerHelper();

            validationRunCtor = new CtorWrapper<IRun<DummyTestContext>>(new[] { validationRun1, validationRun2 });

            containerHelper.Setup(b => b.Register<IRun<DummyTestContext>>(validationRunCtor.Factory));
            containerHelper.Setup(b => b.RegisterGeneric(typeof(ValidateActionSet<>)).As(typeof(IValidateActionSet<>)));
        }

        [TearDown]
        public void TearDown()
        {
            FullFledgedTddModule.ComponentContext = null;
        }

        #endregion

        [Test]
        public void Test1()
        {
            var factory = containerHelper.Container.Resolve<ValidateActionSetFactory<DummyTestContext>>();
            var target = factory(c.actionSet1, actionRecord.Action/*, tearDownActionRecord.Action*/);


            var runs1 = target.GetRuns();
            CollectionAssert.AreEqual(
                new[]
                    {
                        new []{c.run1_1,validationRun1},
                        new []{c.run1_1},
                        new []{c.run2_1,validationRun1},
                        new []{c.run2_1},
                    },
                runs1
            );

            var runs2 = target.GetRuns();
            CollectionAssert.AreEqual(
                new[]
                    {
                        new []{c.run1_2,validationRun2},
                        new []{c.run1_2},
                        new []{c.run2_2,validationRun2},
                        new []{c.run2_2},
                    },
                runs2
            );

            Assert.AreEqual(2, validationRunCtor.Calls.Count());

            foreach (var call in validationRunCtor.Calls)
            {
                Assert.AreEqual(3, call.Count());
                actionTestHelper.VerifyAction(actionRecord, call.Named<Action<DummyTestContext>>("action"));
                Assert.IsNull(call.Named<Action<DummyTestContext>>("tearDownAction"));
                Assert.IsTrue(call.Named<bool>("isValidation"));
            }
        }
    }
}
