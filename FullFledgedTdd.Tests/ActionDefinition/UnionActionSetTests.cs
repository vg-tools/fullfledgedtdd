using Autofac;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.Tests.TestHelpers;
using FullFledgedTdd.Tests.TestUtils;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.ActionDefinition
{
    [TestFixture]
    public class UnionActionSetTests
    {
        #region Setup

        private DummyRunAndDummyActionSetHelper c;
        private ContainerHelper containerHelper;

        [SetUp]
        public void SetUp()
        {
            c = new DummyRunAndDummyActionSetHelper();

            //container setup
            containerHelper = new ContainerHelper();
            containerHelper.Setup(b => b.RegisterGeneric(typeof(UnionActionSet<>)).As(typeof(IUnionActionSet<>)));
        }

        [TearDown]
        public void TearDown()
        {
            FullFledgedTddModule.ComponentContext = null;
        }

        #endregion

        [Test]
        public void GetRunSequencesTest()
        {
            var factory = containerHelper.Container.Resolve<UnionActionSetFactory<DummyTestContext>>();
            var target = factory(new[] { c.actionSet1, c.actionSet2 });


            var runs1 = target.GetRuns();
            CollectionAssert.AreEqual(
                new[]
                    {
                        new []{c.run1_1},
                        new []{c.run2_1},
                        new []{c.run3_1},
                        new []{c.run4_1},
                    },
                runs1
            );

            //Test second call of GetRuns()
            var runs2 = target.GetRuns();
            CollectionAssert.AreEqual(
                new[]
                    {
                        new []{c.run1_2},
                        new []{c.run2_2},
                        new []{c.run3_2},
                        new []{c.run4_2},
                    },
                runs2
            );
        }
    }
}
