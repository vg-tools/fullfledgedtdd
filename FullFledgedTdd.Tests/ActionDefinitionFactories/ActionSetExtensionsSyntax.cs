using System;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.Tests.TestHelpers;
using Moq;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.ActionDefinitionFactories
{
    public class ActionSetExtensionsSyntax
    {
        private readonly ActionSet<DummyTestContext> step1 = Mock.Of<ActionSet<DummyTestContext>>();
        private readonly ActionSet<DummyTestContext> step2 = Mock.Of<ActionSet<DummyTestContext>>();

        #region Create

        public void CreationFromAction()
        {
            var t = (ActionSetInternal<DummyTestContext>)
                
                ActionSetExtensions.Create<DummyTestContext>(
                    c =>
                    {
                        c.P1++;
                    }
                );
        }

        public void CreationFromActionWithTearDownMethod()
        {
            ActionSet<DummyTestContext> t =

                ActionSetExtensions.Create<DummyTestContext>(
                    c =>
                    {
                        //SetUp
                        c.P1 = 1;
                    },
                    c =>
                    {
                        //TearDown
                        c.P1 = 0;
                    }
                );
        }

        #endregion

        #region ToActionSet

        public void OneActionToActionSet()
        {
            Action<DummyTestContext> action = new Action<DummyTestContext>(
                c =>
                {
                    c.P1 = 0;
                }
            );

            ActionSet<DummyTestContext> t = action.ToActionSet();
        }

        #endregion

        #region Combine

        public void CombineOneWithOne()
        {
            ActionSet<DummyTestContext> res = step1.Combine(step2);
        }

        public void CombineOneWithAction()
        {
            ActionSet<DummyTestContext> res = step1.Combine(c => c.P1 = 1);
        }

        #endregion

        #region Union

        public void UnionOneWithOne()
        {
            ActionSet<DummyTestContext> res = step1.Union(step2);
        }

        public void UnionOneWithAction()
        {
            ActionSet<DummyTestContext> res = step1.Union(c => c.P1 = 1);
        }

        #endregion

        #region Validate

        public void Validate()
        {
            ActionSet<DummyTestContext> res = step1.Validate(c => Assert.AreEqual(0, c.P1));
        }

        #endregion
    }
}