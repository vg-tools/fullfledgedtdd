using System;
using System.Linq;
using System.Collections.Generic;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.Tests.TestHelpers;
using FullFledgedTdd.Tests.TestUtils;
using Moq;
using NUnit.Framework;
using Autofac;

namespace FullFledgedTdd.Tests.ActionDefinitionFactories
{
    [TestFixture]
    public class ActionSetExtensionsTests
    {
        #region Setup

        private DummyRunAndDummyActionSetHelper c;
        private ContainerHelper containerHelper;
        private ActionTestHelper actionTestHelper;
        private ActionTestHelper.ActionRecord<DummyTestContext> actionRecord;
        private ActionTestHelper.ActionRecord<DummyTestContext> tearDownActionRecord;

        private ILeafActionSet<DummyTestContext> leafActionSet;
        private ICombinationActionSet<DummyTestContext> combinationActionSet;
        private IUnionActionSet<DummyTestContext> unionActionSet;
        private IValidateActionSet<DummyTestContext> validateActionSet;

        private CtorWrapper<ILeafActionSet<DummyTestContext>> leafActionSetCtor;
        private CtorWrapper<ICombinationActionSet<DummyTestContext>> combinationActionSetCtor;
        private CtorWrapper<IUnionActionSet<DummyTestContext>> unionActionSetCtor;
        private CtorWrapper<IValidateActionSet<DummyTestContext>> validateActionSetCtor;

        [SetUp]
        public void SetUp()
        {
            c = new DummyRunAndDummyActionSetHelper();
            actionTestHelper = new ActionTestHelper();
            actionRecord = actionTestHelper.CreateAction<DummyTestContext>();
            tearDownActionRecord = actionTestHelper.CreateAction<DummyTestContext>();

            //Create container
            containerHelper = new ContainerHelper();
            FullFledgedTddModule.ComponentContext = containerHelper.Container;

            leafActionSet = Mock.Of<ILeafActionSet<DummyTestContext>>();
            combinationActionSet = Mock.Of<ICombinationActionSet<DummyTestContext>>();
            unionActionSet = Mock.Of<IUnionActionSet<DummyTestContext>>();
            validateActionSet = Mock.Of<IValidateActionSet<DummyTestContext>>();

            leafActionSetCtor = new CtorWrapper<ILeafActionSet<DummyTestContext>>(leafActionSet);
            combinationActionSetCtor = new CtorWrapper<ICombinationActionSet<DummyTestContext>>(combinationActionSet);
            unionActionSetCtor = new CtorWrapper<IUnionActionSet<DummyTestContext>>(unionActionSet);
            validateActionSetCtor = new CtorWrapper<IValidateActionSet<DummyTestContext>>(validateActionSet);

            containerHelper.Setup(b => b.Register<ILeafActionSet<DummyTestContext>>(leafActionSetCtor.Factory));
            containerHelper.Setup(b => b.Register(combinationActionSetCtor.Factory));
            containerHelper.Setup(b => b.Register(unionActionSetCtor.Factory));
            containerHelper.Setup(b => b.Register(validateActionSetCtor.Factory));
        }

        [TearDown]
        public void TearDown()
        {
            FullFledgedTddModule.ComponentContext = null;
        }

        #endregion

        #region Create

        [Test]
        public void CreationFromAction()
        {
            var res = ActionSetExtensions.Create<DummyTestContext>(actionRecord.Action);


            Assert.AreSame(leafActionSet, res);

            var call = leafActionSetCtor.Calls.Single();
            Assert.AreEqual(1, call.Count());
            actionTestHelper.VerifyAction(actionRecord, call.Named<Action<DummyTestContext>>("action"));
        }

        [Test]
        public void CreationFromActionWithTearDownMethod()
        {
            var res = ActionSetExtensions.Create<DummyTestContext>(actionRecord.Action, tearDownActionRecord.Action);


            Assert.AreSame(leafActionSet, res);

            var call = leafActionSetCtor.Calls.Single();

            Assert.AreEqual(2, call.Count());
            actionTestHelper.VerifyAction(actionRecord, call.Named<Action<DummyTestContext>>("action"));
            actionTestHelper.VerifyAction(tearDownActionRecord, call.Named<Action<DummyTestContext>>("tearDownAction"));
        }

        #endregion

        #region ToActionSet

        [Test]
        public void OneActionToActionSet()
        {
            var res = actionRecord.Action.ToActionSet();


            Assert.AreSame(leafActionSet, res);

            var call = leafActionSetCtor.Calls.Single();
            Assert.AreEqual(1, call.Count());
            actionTestHelper.VerifyAction(actionRecord, call.Named<Action<DummyTestContext>>("action"));
        }

        #endregion

        #region Combine

        [Test]
        public void CombineOneWithOne()
        {
            var actionSet1 = Mock.Of<ActionSet<DummyTestContext>>();
            var actionSet2 = Mock.Of<ActionSet<DummyTestContext>>();

            
            var actualCombination = actionSet1.Combine(actionSet2);


            Assert.AreSame(combinationActionSet, actualCombination);

            var call = combinationActionSetCtor.Calls.Single();

            Assert.AreEqual(1, call.Count());

            CollectionAssert.AreEqual(
                new[] { actionSet1, actionSet2 },
                call.Named<IEnumerable<ActionSet<DummyTestContext>>>("actionSets")
            );
        }

        [Test]
        public void CombineOneWithAction()
        {
            var actionSet = Mock.Of<ActionSet<DummyTestContext>>();


            var actualCombination = actionSet.Combine(actionRecord.Action);


            Assert.AreSame(combinationActionSet, actualCombination);

            var combinationCall = combinationActionSetCtor.Calls.Single();

            Assert.AreEqual(1, combinationCall.Count());

            var actionSetsParameter = combinationCall.Named<IEnumerable<ActionSet<DummyTestContext>>>("actionSets");

            CollectionAssert.AreEqual(
                new[] { actionSet, leafActionSet },
                actionSetsParameter
            );

            Assert.AreSame(leafActionSet, actionSetsParameter.ElementAt(1));

            var leafCall = leafActionSetCtor.Calls.Single();

            Assert.AreEqual(1, leafCall.Count());
            actionTestHelper.VerifyAction(actionRecord, leafCall.Named<Action<DummyTestContext>>("action"));
        }

        #endregion

        #region Union

        [Test]
        public void UnionOneWithOne()
        {
            var actionSet1 = Mock.Of<ActionSet<DummyTestContext>>();
            var actionSet2 = Mock.Of<ActionSet<DummyTestContext>>();


            var actualUnion = actionSet1.Union(actionSet2);


            Assert.AreSame(unionActionSet, actualUnion);

            var unionCall = unionActionSetCtor.Calls.Single();

            Assert.AreEqual(1, unionCall.Count());

            CollectionAssert.AreEqual(
                new[] { actionSet1, actionSet2 },
                unionCall.Named<IEnumerable<ActionSet<DummyTestContext>>>("actionSets")
            );
        }

        [Test]
        public void UnionOneWithAction()
        {
            var actionSet = Mock.Of<ActionSet<DummyTestContext>>();
            var actionRecord = actionTestHelper.CreateAction<DummyTestContext>();


            var actualUnion = actionSet.Union(actionRecord.Action);


            Assert.AreSame(unionActionSet, actualUnion);

            var unionCall = unionActionSetCtor.Calls.Single();

            Assert.AreEqual(1, unionCall.Count());

            var actionSetsParameter = unionCall.Named<IEnumerable<ActionSet<DummyTestContext>>>("actionSets");

            CollectionAssert.AreEqual(
                new[] { actionSet, leafActionSet },
                actionSetsParameter
            );

            Assert.AreSame(leafActionSet, actionSetsParameter.ElementAt(1));

            var leafCall = leafActionSetCtor.Calls.Single();

            Assert.AreEqual(1, leafCall.Count());
            actionTestHelper.VerifyAction(actionRecord, leafCall.Named<Action<DummyTestContext>>("action"));
        }

        #endregion

        #region Validate

        [Test]
        public void ValidateWithOneAction()
        {
            var actionSet = Mock.Of<ActionSet<DummyTestContext>>();
            var actionRecord = actionTestHelper.CreateAction<DummyTestContext>();


            var actualValidation = actionSet.Validate(actionRecord.Action);


            Assert.AreSame(validateActionSet, actualValidation);

            var validationCall = validateActionSetCtor.Calls.Single();

            Assert.AreEqual(2, validationCall.Count());

            Assert.AreEqual(
                actionSet,
                validationCall.Named<ActionSet<DummyTestContext>>("actionSet")
            );

            actionTestHelper.VerifyAction(actionRecord, validationCall.Named<Action<DummyTestContext>>("action"));
            /*Assert.IsNull(validationCall.Named<Action<DummyTestContext>>("tearDownAction"));*/
        }

        /*[Test]
        public void ValidateWithActionAndTearDownAction()
        {
            var actionSet = Mock.Of<ActionSet<DummyTestContext>>();
            var actionRecord = actionTestHelper.CreateAction<DummyTestContext>();
            var tearDownActionRecord = actionTestHelper.CreateAction<DummyTestContext>();

            
            var actualValidation = actionSet.Validate(actionRecord.Action, tearDownActionRecord.Action);


            Assert.AreSame(validateActionSet, actualValidation);

            var validationCall = validateActionSetCtor.Calls.Single();

            Assert.AreEqual(3, validationCall.Count());

            Assert.AreEqual(
                actionSet,
                validationCall.Named<ActionSet<DummyTestContext>>("actionSet")
            );

            actionTestHelper.VerifyAction(actionRecord, validationCall.Named<Action<DummyTestContext>>("action"));
            actionTestHelper.VerifyAction(tearDownActionRecord, validationCall.Named<Action<DummyTestContext>>("tearDownAction"));
        }*/

        #endregion
    }
}