using System;
using System.Collections.Generic;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.TestDefinition;
using FullFledgedTdd.Tests.TestHelpers;
using NUnit.Framework;
using System.Linq;

namespace FullFledgedTdd.Tests.ActionDefinitionFactories
{
    public class ActionSetFactoryTests
    {
        private ActionSetFactory<DummyTestContext> target;
        private DummyTestContext userContext;

        [SetUp]
        public void SetUp()
        {
            target = new ActionSetFactory<DummyTestContext>();
            userContext = new DummyTestContext();
        }

        [Test]
        public void Create()
        {
            var actionSet = target.Create(c => c.P1 = 1);
            ExecuteAction(actionSet, userContext);

            Assert.AreEqual(1, userContext.P1);
        }

        [Test]
        public void SetProperty()
        {
            var actionSet = target.SetProperty(new[] { 10, 11, 12 }, c => c.P1);

            Assert.AreEqual(3, GetActionCount(actionSet));

            ExecuteAction(actionSet, 0, userContext);
            Assert.AreEqual(10, userContext.P1);

            ExecuteAction(actionSet, 1, userContext);
            Assert.AreEqual(11, userContext.P1);

            ExecuteAction(actionSet, 2, userContext);
            Assert.AreEqual(12, userContext.P1);
        }

        [Test]
        public void AnyInt()
        {
            var actionSet = target.AnyInt(c => c.P1);
            var runs = GetActions(actionSet, userContext);

            var values = runs.Select(
                r =>
                {
                    r();
                    return userContext.P1;
                }).ToList();

            CollectionAssert.AreEqual(
                new[] { 0, 1, -1, 100, -100 },
                values
            );
        }

        [Test]
        public void AnyCount()
        {
            var actionSet = target.AnyCount(c => c.P1);
            var runs = GetActions(actionSet, userContext);

            var values = runs.Select(
                r =>
                {
                    r();
                    return userContext.P1;
                }).ToList();

            CollectionAssert.AreEqual(
                new[] { 0, 1, 2, 5, 10, 100 },
                values
            );
        }

        #region Help methods

        private int GetActionCount<T>(ActionSet<T> actionSet)
        {
            Assert.IsInstanceOf<ActionSetInternal<T>>(actionSet);
            var actionSetInternal = (ActionSetInternal<T>)actionSet;

            var runs = actionSetInternal.GetRuns();
            return runs.Length;
        }

        private static void ExecuteAction<T>(ActionSet<T> actionSet, T userContext)
        {
            Assert.IsInstanceOf<ActionSetInternal<T>>(actionSet);
            var actionSetInternal = (ActionSetInternal<T>)actionSet;

            var runs = actionSetInternal.GetRuns();

            Assert.AreEqual(1, runs.Length);
            Assert.AreEqual(1, runs[0].Count());
            var run = runs[0].ToArray()[0];

            var runSequenceExecutionContext = new RunSequenceExecutionContext<T>() { UserContext = userContext };
            run.Execute(runSequenceExecutionContext);
        }

        private static void ExecuteAction<T>(ActionSet<T> actionSet, int index, T userContext)
        {
            Assert.IsInstanceOf<ActionSetInternal<T>>(actionSet);
            var actionSetInternal = (ActionSetInternal<T>)actionSet;

            var runSequence = actionSetInternal.GetRuns()[index].ToArray();

            Assert.AreEqual(1, runSequence.Count());
            var run = runSequence[0];

            var runSequenceExecutionContext = new RunSequenceExecutionContext<T>() { UserContext = userContext };
            run.Execute(runSequenceExecutionContext);
        }

        private IEnumerable<Action> GetActions<T>(ActionSet<T> actionSet, T userContext)
        {
            Assert.IsInstanceOf<ActionSetInternal<T>>(actionSet);
            var actionSetInternal = (ActionSetInternal<T>)actionSet;

            var runSequenceExecutionContext = new RunSequenceExecutionContext<T>() { UserContext = userContext };

            var result = new List<Action>();
            var runSequences = actionSetInternal.GetRuns();
            foreach (var runSequence in runSequences)
            {
                var runs = runSequence.ToArray();
                Assert.AreEqual(1, runs.Length);
                result.Add(() => runs[0].Execute(runSequenceExecutionContext));
            }
            return result.ToArray();
        }

        #endregion
    }
}