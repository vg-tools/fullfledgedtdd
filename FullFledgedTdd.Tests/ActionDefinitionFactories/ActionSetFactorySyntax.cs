using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.Tests.TestHelpers;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.ActionDefinitionFactories
{
    //[TestFixture]
    public class ActionSetFactorySyntax
    {
        private ActionSetFactory<DummyTestContext> target;

        [SetUp]
        public void SetUp()
        {
            target = new ActionSetFactory<DummyTestContext>();
        }

        //[Test]
        public void Create()
        {
            ActionSet<DummyTestContext> t =

                target.Create(c => c.P1 = 1);
        }

        //[Test]
        public void SetProperty()
        {
            ActionSet<DummyTestContext> t =

                target.SetProperty(new[] { 0, 1, 2 }, c => c.P1);
        }
    }
}