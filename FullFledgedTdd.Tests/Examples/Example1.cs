﻿using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.TestDefinition;
using NUnit.Framework;
using Autofac;

namespace FullFledgedTdd.Tests.Examples
{
    public class Example1
    {
        [SetUp]
        public void SetUp()
        {
            
        }

        [Test]
        public void Complex1()
        {
            var testSetFactory = FullFledgedTddModule.ComponentContext.Resolve<TestSetFactory<Example1>>();
            var tests = testSetFactory("", () => this, c => { });

            var f = FullFledgedTddModule.ComponentContext.Resolve<ActionSetFactory<Example1>>();
            var target = new ClassToTest();

            tests.Add(
                f.Create(c =>
                    {
                        a = 1;
                        b = 2;
                        sum = 3;
                    }
                ).Union(c =>
                    {
                        a = 200;
                        b = 500;
                        sum = 700;
                    }
                )
                .Validate(c => Assert.AreEqual(sum, target.Sum(a, b)))
            );
        }

        public int a, b, sum;

        [Test]
        public void Complex2()
        {
            var testSetFactory = FullFledgedTddModule.ComponentContext.Resolve<TestSetFactory<TestContext>>();
            var tests = testSetFactory("");
            var f = FullFledgedTddModule.ComponentContext.Resolve<ActionSetFactory<TestContext>>();

            tests.Add(
                f.AnyInt(c => c.A) & f.AnyInt(c => c.B)
                .Validate(c => Assert.AreEqual(c.A + c.B, c.Target.Sum(c.A, c.B)))
            );
        }

        class TestContext
        {
            public readonly ClassToTest Target = new ClassToTest();

            public int A, B;
        }

        class ClassToTest
        {
            public int Sum(int a, int b)
            {
                return a + b;
            }
        }
    }
}
