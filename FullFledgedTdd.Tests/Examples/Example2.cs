using System;
using Autofac;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.TestDefinition;
using Moq;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.Examples
{
    public class Example2
    {
        private ITestSet<TestContext> testManager;
        private ActionSetFactory<TestContext> f;
        private ActionSet<TestContext> setupActionSet;

        [SetUp]
        public void NUnitSetUp()
        {
            testManager = FullFledgedTddModule.ComponentContext.Resolve<TestSetFactory<TestContext>>()("", () => new TestContext(), c => c.Dispose());
            f = FullFledgedTddModule.ComponentContext.Resolve<ActionSetFactory<TestContext>>();
        }

        [Test]
        public void Test()
        {
            var anyPort = f.SetProperty(new[] { 100, 1000 }, c => c.PortParam);
            var anyName = f.AnyText(c => c.NameParam);

            var createTargetWithSpecificPort = f.Create(c => c.Target = new Target(c.NameParam, c.PortParam), c => c.Target.Dispose());
            var createTargetWithDefaultPort = f.Create(c => c.Target = new Target(c.NameParam), c => c.Target.Dispose());

            testManager.Add(
                anyName &
                (
                    anyPort & createTargetWithSpecificPort
                    .Validate(c => Assert.AreEqual(c.PortParam, c.Target.Port))
                    |
                    createTargetWithDefaultPort
                    .Validate(c => Assert.AreEqual(8080, c.Target.Port))
                )
                .Validate(c => Assert.AreEqual(c.NameParam, c.Target.Name))
            );
        }

        public class TestContext : IDisposable
        {
            public string NameParam { get; set; }
            public int PortParam { get; set; }

            public Target Target { get; set; }

            public TestContext()
            {
            }

            public void Dispose()
            {
            }
        }

        public class Target : IDisposable
        {
            public string Name { get; set; }
            public int Port { get; set; }

            public Target(string name, int port)
            {
                Name = name;
                Port = port;
            }

            public Target(string name) : this(name, 8080)
            {
            }

            public void Dispose()
            {
            }
        }

    }
}