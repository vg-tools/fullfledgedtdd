﻿using System;
using Autofac;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.TestDefinition;
using Moq;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.Examples
{
    public class Example3
    {
        private ITestSet<TestContext> testManager;
        private ActionSetFactory<TestContext> f;
        private ActionSet<TestContext> setupActionSet;

        [SetUp]
        public void NUnitSetUp()
        {
            testManager = FullFledgedTddModule.ComponentContext.Resolve<TestSetFactory<TestContext>>()("");
            f = FullFledgedTddModule.ComponentContext.Resolve<ActionSetFactory<TestContext>>();
            setupActionSet = f.Create(Setup, TearDown);
        }

        [Test]
        public void TestOutputNodeName()
        {
            var anyInputNodeName = f.Create(c => { c.InputNodeName = "123"; }).Union(c => { c.InputNodeName = "234"; });

            testManager.Add(
                "Input node name provided in ctor",
                setupActionSet
                .Combine(anyInputNodeName)
                .Combine(CreateTargetWithInputNodeNameParameter)
                .Validate(c => Assert.AreEqual(c.InputNodeName, c.Target.Name))
            );

            testManager.Add(
                "Default input node name",
                setupActionSet
                .Combine(anyInputNodeName)
                .Combine(CreateTargetWithDefaultInputNodeName)
                .Validate(c => Assert.AreEqual("JoinedOrder", c.Target.Name))
            );
        }

        [Test]
        public void TestBWManagerEventSubscription()
        {
            var anyInputNodeName = f.Create(c => { c.InputNodeName = "123"; }).Union(c => { c.InputNodeName = "234"; });
            var anyConstructor = f.Create(CreateTargetWithInputNodeNameParameter).Union(CreateTargetWithDefaultInputNodeName);
            var createTargetInAnyWay = setupActionSet & anyInputNodeName & anyConstructor;

            testManager.Add(
                createTargetInAnyWay
                .Combine(c => { c.Target.StatusChanged += (s, e) => c.calls++; })
                .Validate(
                    c =>
                    {
                        c.Manager.Raise(m => m.StatusChanged += null, EventArgs.Empty);
                        Assert.AreEqual(1, c.calls);
                    }
                )
                .Validate(
                    c =>
                    {
                        c.Target.Dispose();

                        c.Manager.Raise(m => m.StatusChanged += null, EventArgs.Empty);
                        Assert.AreEqual(0, c.calls);
                    }
                )
            );
        }

        void CreateTargetWithInputNodeNameParameter(TestContext c)
        {
            c.Target = new StaticDataHolder(c.InputNodeName, c.Manager.Object);
        }

        void CreateTargetWithDefaultInputNodeName(TestContext c)
        {
            c.Target = new StaticDataHolder(c.Manager.Object);
        }

        void Setup(TestContext c)
        {
            c.Manager = new Mock<IBWManager>();
        }

        void TearDown(TestContext c)
        {
        }

        public class TestContext
        {
            public Mock<IBWManager> Manager;
            public string InputNodeName;
            public StaticDataHolder Target;
            public int calls = 0;
        }

        public enum ConnectionStatus
        {
            Up,
            Down,
        }

        public interface IDataHolder
        {
            string Name { get; }
            ConnectionStatus Status { get; }
            event EventHandler StatusChanged;
        }

        public interface IBWManager
        {
            ConnectionStatus Status { get; }
            event EventHandler StatusChanged;
        }

        public class StaticDataHolder : IDataHolder, IDisposable
        {
            private readonly IBWManager bwManager;

            public StaticDataHolder(IBWManager bwManager)
            {
                this.bwManager = bwManager;

                Name = "JoinedOrder";
                Status = bwManager.Status;

                bwManager.StatusChanged += OnBWManagerStatusChanged;
            }

            public StaticDataHolder(string nodeName, IBWManager bwManager)
            {
                this.bwManager = bwManager;

                Name = nodeName;
                Status = bwManager.Status;

                bwManager.StatusChanged += OnBWManagerStatusChanged;
            }

            public string Name { get; private set; }
            public ConnectionStatus Status { get; private set; }
            public event EventHandler StatusChanged;

            void OnBWManagerStatusChanged(object sender, EventArgs e)
            {
                Status = bwManager.Status;
                if (StatusChanged != null)
                {
                    StatusChanged(this, EventArgs.Empty);
                }
            }

            public void Dispose()
            {
                bwManager.StatusChanged -= OnBWManagerStatusChanged;
            }
        }

        public class NodeDataHolder : IDataHolder, IDisposable
        {
            private readonly IDataHolder parentDataHolder;

            public NodeDataHolder(IDataHolder parentDataHolder)
            {
                this.parentDataHolder = parentDataHolder;
                Status = parentDataHolder.Status;
                parentDataHolder.StatusChanged += OnParentDataHolderStatusChanged;
            }

            public string Name { get; private set; }
            public ConnectionStatus Status { get; private set; }
            public event EventHandler StatusChanged;

            void OnParentDataHolderStatusChanged(object sender, EventArgs e)
            {
                Status = parentDataHolder.Status;
                if (StatusChanged != null)
                {
                    StatusChanged(this, EventArgs.Empty);
                }
            }

            public void Dispose()
            {
                parentDataHolder.StatusChanged -= OnParentDataHolderStatusChanged;
            }
        }
    }
}