namespace FullFledgedTdd.Tests.TestHelpers
{
    public class DummyRunAndDummyActionSetHelper
    {
        public DummyRun run1_1;
        public DummyRun run1_2;
        public DummyRun run2_1;
        public DummyRun run2_2;
        public DummyRun run3_1;
        public DummyRun run3_2;
        public DummyRun run4_1;
        public DummyRun run4_2;

        public DummyActionSet<DummyTestContext> actionSet1;
        public DummyActionSet<DummyTestContext> actionSet2;

        public DummyRunAndDummyActionSetHelper()
        {
            run1_1 = new DummyRun { Key = "1_1" };
            run1_2 = new DummyRun { Key = "1_2" };
            run2_1 = new DummyRun { Key = "2_1" };
            run2_2 = new DummyRun { Key = "2_2" };
            run3_1 = new DummyRun { Key = "3_1" };
            run3_2 = new DummyRun { Key = "3_2" };
            run4_1 = new DummyRun { Key = "4_1" };
            run4_2 = new DummyRun { Key = "4_2" };

            actionSet1 = new DummyActionSet<DummyTestContext>
            {
                PageSize = 2,
                Runs = new[] { new[] { run1_1 }, new[] { run2_1 }, new[] { run1_2 }, new[] { run2_2 } }
            };
            actionSet2 = new DummyActionSet<DummyTestContext>
            {
                PageSize = 2,
                Runs = new[] { new[] { run3_1 }, new[] { run4_1 }, new[] { run3_2 }, new[] { run4_2 } }
            };
        }
    }
}