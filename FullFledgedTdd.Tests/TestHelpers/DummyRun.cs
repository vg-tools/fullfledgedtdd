using System;
using FullFledgedTdd.TestDefinition;

namespace FullFledgedTdd.Tests.TestHelpers
{
    public class DummyRun : IRun<DummyTestContext>
    {
        public string Key { get; set; }

        #region Not used

        public bool IsValidation
        {
            get { throw new NotImplementedException(); }
        }

        public bool Execute(RunSequenceExecutionContext<DummyTestContext> runSequenceExecutionContext)
        {
            throw new NotImplementedException();
        }

        public bool TearDown(RunSequenceExecutionContext<DummyTestContext> runSequenceExecutionContext)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}