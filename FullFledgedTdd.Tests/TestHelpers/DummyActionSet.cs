using System.Collections.Generic;
using System.Linq;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.TestDefinition;

namespace FullFledgedTdd.Tests.TestHelpers
{
    public class DummyActionSet<T> : ActionSetInternal<T>
    {
        private IEnumerable<IEnumerable<IRun<T>>> runs;

        public IEnumerable<IEnumerable<IRun<T>>> Runs = new List<IEnumerable<IRun<T>>>();
        public int PageSize = 1;

        public override IEnumerable<IRun<T>>[] GetRuns()
        {
            if (runs == null)
            {
                runs = Runs;
            }
            var res = runs.Take(PageSize).ToArray();
            runs = runs.Skip(PageSize).ToArray();

            return res;
        }
    }
}