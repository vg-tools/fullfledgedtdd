using System;
using System.Text;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.TestDefinition;
using FullFledgedTdd.Tests.TestHelpers;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.TestDefinition
{
    [TestFixture]
    public class TestSetTests
    {
        #region Setup

        private TestSet<DummyTestContext> target;
        private StringBuilder sb;

        private ActionSet<DummyTestContext> actionSet1;
        private ActionSet<DummyTestContext> actionSet2;
        private ActionSet<DummyTestContext> actionSet3;

        private Action<DummyTestContext> validationAction1;
        private Action<DummyTestContext> validationAction2;

        private ActionSet<DummyTestContext> failingActionSet;
        private ActionSet<DummyTestContext> failingTearDownActionSet;
        private Action<DummyTestContext> validationFails;

        [SetUp]
        public void SetUp()
        {
            sb = new StringBuilder();

            actionSet1 =
                ActionSetExtensions.Create<DummyTestContext>(c => { c.P1 = 1; sb.Append("a1 "); }, c => sb.Append("~a1 "))
                .Union(c => sb.Append("a2 "));

            failingActionSet = ActionSetExtensions.Create<DummyTestContext>(c => Assert.Fail(), c => sb.Append("~f1 "));
            failingTearDownActionSet = ActionSetExtensions.Create<DummyTestContext>(c => sb.Append("f1 "), c => Assert.Fail());

            actionSet2 =
                ActionSetExtensions.Create<DummyTestContext>(c => sb.Append("b1 "), c => sb.Append("~b1 "))
                .Union(c => sb.Append("b2 "));

            actionSet3 =
                ActionSetExtensions.Create<DummyTestContext>(c => sb.Append("c1 "), c => sb.Append("~c1 "))
                .Union(c => sb.Append("c2 "));

            validationAction1 = c => sb.Append("v1 ");
            validationAction2 = c => sb.Append("v2 ");

            validationFails = c => { throw new Exception(); };
        }

        #endregion

        #region Context creator or disposer fails

        [Test, ExpectedException(typeof(Exception))]
        public void ContextCreatorFails()
        {
            target = new TestSet<DummyTestContext>(
                "set name",
                () =>
                {
                    throw new Exception();
                },
                DummyUserContextDisposer);

            target.Add(
                "Test1",
                actionSet1
                .Validate(validationAction1)
            );

            Assert.AreEqual("", sb.ToString());
        }

        [Test, ExpectedException(typeof(Exception))]
        public void ContextDisposerFails()
        {
            target = new TestSet<DummyTestContext>(
                "set name",
                CreateDummyUserContext,
                c =>
                {
                    throw new Exception();
                });

            target.Add(
                "Test1",
                actionSet1
                .Validate(validationAction1)
            );

            Assert.AreEqual("ctor ", sb.ToString());
        }

        #endregion

        #region Setup or teardown fails

        [Test, ExpectedException(typeof(AssertionException))]
        public void ExceptionInAction()
        {
            Ctor_ContextCreatorDisposer();

            target.Add(
                "Test1",
                actionSet1
                .Combine(failingActionSet)
                .Validate(validationAction1)
            );

            var expectedString = @"ctor disp
ctor a1 ctor a2 ";
            Assert.AreEqual(expectedString, sb.ToString());
        }

        [Test, ExpectedException(typeof(AssertionException))]
        public void ExceptionInTearDownAction()
        {
            Ctor_ContextCreatorDisposer();

            target.Add(
                "Test1",
                actionSet1
                .Combine(failingTearDownActionSet)
                .Validate(validationAction1)
            );

            var expectedString = @"ctor disp
ctor a1 f1 v1 ctor a2 f1 v1 ";
            Assert.AreEqual(expectedString, sb.ToString());
        }

        #endregion

        #region Doesn't throw

        [Test]
        public void Test1()
        {
            Ctor_ContextCreatorDisposer();

            target.Add(
                "Test1",
                actionSet1
                .Validate(validationAction1)
                .Combine(actionSet2)
                .Validate(validationAction2)
                .Combine(actionSet3)
            );

            var expectedString = @"ctor a1 v1 ~a1 disp
ctor a1 b1 v2 ~b1 ~a1 disp
ctor a1 b2 v2 ~a1 disp
ctor a2 v1 disp
ctor a2 b1 v2 ~b1 disp
ctor a2 b2 v2 disp
";
            Assert.AreEqual(expectedString, sb.ToString());
        }

        [Test]
        public void Test2()
        {
            Ctor_ContextCreatorNullDisposer();

            target.Add(
                "Test1",
                actionSet1
                .Validate(validationAction1)
                .Combine(actionSet2)
                .Validate(validationAction2)
                .Combine(actionSet3)
            );

            var expectedString = @"ctor a1 v1 ~a1 ctor a1 b1 v2 ~b1 ~a1 ctor a1 b2 v2 ~a1 ctor a2 v1 ctor a2 b1 v2 ~b1 ctor a2 b2 v2 ";
            Assert.AreEqual(expectedString, sb.ToString());
        }

        [Test]
        public void Test3()
        {
            Ctor_NullContextCreatorNullDisposer();

            target.Add(
                "Test1",
                actionSet1
                .Validate(validationAction1)
                .Combine(actionSet2)
                .Validate(validationAction2)
                .Combine(actionSet3)
            );

            var expectedString = @"a1 v1 ~a1 a1 b1 v2 ~b1 ~a1 a1 b2 v2 ~a1 a2 v1 a2 b1 v2 ~b1 a2 b2 v2 ";
            Assert.AreEqual(expectedString, sb.ToString());
        }

        [Test]
        public void NoValidationRegistered()
        {
            Ctor_ContextCreatorDisposer();

            target.Add(
                "Test1",
                actionSet1
                .Combine(actionSet2)
                .Combine(actionSet3)
            );

            var expectedString = @"";
            Assert.AreEqual(expectedString, sb.ToString());
        }

        #endregion

        #region Routines

        void Ctor_NullContextCreatorNullDisposer()
        {
            target = new TestSet<DummyTestContext>("set name", null, null);
        }

        void Ctor_ContextCreatorNullDisposer()
        {
            target = new TestSet<DummyTestContext>("set name", CreateDummyUserContext, null);
        }

        void Ctor_ContextCreatorDisposer()
        {
            target = new TestSet<DummyTestContext>("set name", CreateDummyUserContext, DummyUserContextDisposer);
        }

        DummyTestContext CreateDummyUserContext()
        {
            sb.Append("ctor ");
            return new DummyTestContext();
        }

        void DummyUserContextDisposer(DummyTestContext c)
        {
            sb.AppendLine("disp");
        }

        #endregion

        public class DummyUserContext2 : IDisposable
        {
            public int DisposeCalls = 0;

            public void Dispose()
            {
                DisposeCalls++;
            }
        }
    }
}
