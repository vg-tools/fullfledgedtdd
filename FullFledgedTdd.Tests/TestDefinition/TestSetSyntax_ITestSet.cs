using System.Linq;
using Autofac;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.TestDefinition;
using FullFledgedTdd.Tests.TestHelpers;
using Moq;
using NUnit.Framework;

namespace FullFledgedTdd.Tests.TestDefinition
{
    public class TestSetSyntax_ITestSet
    {
        private DummyRunAndDummyActionSetHelper c;
        private IComponentContext componentContext;

        public void SetUp()
        {
            c = new DummyRunAndDummyActionSetHelper();
            componentContext = Mock.Of<IComponentContext>();
        }

        public void Factory()
        {
            var factory = componentContext.Resolve<TestSetFactory<DummyTestContext>>();

            var testSet = factory("Test set 1", () => new DummyTestContext());
            var testSet2 = factory("Test set 2", () => new DummyTestContext(), context => {/* dispose context*/});

            testSet.Add(c.actionSet1);
            testSet.Add("Named test", c.actionSet2);
        }

        public void Complex1()
        {
            var f = new ActionSetFactory<DummyTestContext>();
            var factory = componentContext.Resolve<TestSetFactory<DummyTestContext>>();
            var testSet = factory("Test set 1", () => new DummyTestContext());

            var anyP1 = f.AnyInt(c => c.P1);
            var smallP1 = f.SetProperty(Enumerable.Range(1, 5), c => c.P1);
            var bigP1 = f.SetProperty(new[] { 100, 200 }, c => c.P1);

            var smallP2 = f.SetProperty(Enumerable.Range(1, 5), c => c.P2);
            var anyP2 = f.AnyInt(c => c.P2);
            var anyP3 = f.AnyInt(c => c.P2);

            testSet.Add(
                (
                    (smallP1 & smallP2) |
                    (bigP1 & anyP2)
                )
                .Validate(
                    c =>
                    {
                        Assert.IsTrue(c.P1 > 0);
                        Assert.IsTrue(c.P1 < 6);
                    }
                )
                .Validate(c => Assert.IsTrue(c.P2 > 0))
                .Combine(anyP3)
                .Validate(c => Assert.IsTrue(c.P3 > 0))
            );
        }
    }
}