using System;

namespace FullFledgedTdd.TestDefinition
{
    public delegate IRun<T> RunFactory<T>(
        Action<T> action,
        Action<T> tearDownAction,
        bool isValidation);

    //todo: Implement LeafActionSet and ValidationActionSet with advanced function. Implement extention methods, methods in ActionSetFactory.
    //    public delegate IRun<T> AdvancedRunFactory<T>(
    //        Action<T, IRunExecutionContext<T>> advancedAction,
    //        Action<T> tearDownAction,
    //        bool isValidation);

    public interface IRun<T>
    {
        string Key { get; }
        bool IsValidation { get; }

        bool Execute(RunSequenceExecutionContext<T> runSequenceExecutionContext);
        bool TearDown(RunSequenceExecutionContext<T> runSequenceExecutionContext);
    }
}