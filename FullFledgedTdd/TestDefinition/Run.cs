using System;

namespace FullFledgedTdd.TestDefinition
{
    public class Run<T> : IRun<T>
    {
        private readonly string key;
        private readonly Action<T> action;
        private readonly Action<T> tearDownAction;
        private readonly bool isValidation;

        public Run(
            Action<T> action,
            Action<T> tearDownAction,
            bool isValidation)
        {
            if (action == null) throw new ArgumentNullException("action");

            this.action = action;
            this.tearDownAction = tearDownAction;
            this.isValidation = isValidation;

            key = Guid.NewGuid().ToString();
        }

        public string Key
        {
            get { return key; }
        }

        public bool IsValidation
        {
            get { return isValidation; }
        }

        public bool Execute(RunSequenceExecutionContext<T> runSequenceExecutionContext)
        {
            action(runSequenceExecutionContext.UserContext);
            return true;
        }

        public bool TearDown(RunSequenceExecutionContext<T> runSequenceExecutionContext)
        {
            if (tearDownAction != null)
            {
                tearDownAction(runSequenceExecutionContext.UserContext);
            }
            return true;
        }
    }
}