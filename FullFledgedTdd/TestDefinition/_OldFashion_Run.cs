using System;

namespace FullFledgedTdd.TestDefinition
{
    public class _OldFashion_Run<T> : IRun<T>
    {
        private string key;
        private readonly Action<T> action;
        private readonly Action<T, IRunExecutionContext<T>> advancedAction;
        private readonly Action<T> tearDownAction;
        private readonly bool isValidation;

        private readonly Func<RunSequenceExecutionContext<T>, bool> executeFunc;
        private readonly Func<RunSequenceExecutionContext<T>, bool> tearDownFunc;

        public _OldFashion_Run(
            Action<T> action,
            Action<T> tearDownAction,
            bool isValidation)
        {
            if (action == null) throw new ArgumentNullException("action");

            this.action = action;
            executeFunc = DoExecuteAction;

            if (tearDownAction != null)
            {
                this.tearDownAction = tearDownAction;
                tearDownFunc = DoTearDown;
            }
            else
            {
                tearDownFunc = c => true;
            }

            this.isValidation = isValidation;

            key = Guid.NewGuid().ToString();
        }

        public _OldFashion_Run(
            Action<T, IRunExecutionContext<T>> advancedAction,
            Action<T> tearDownAction,
            bool isValidation)
        {
            if (advancedAction == null) throw new ArgumentNullException("advancedAction");

            this.advancedAction = advancedAction;
            executeFunc = DoExecuteAdvancedAction;

            if (tearDownAction != null)
            {
                this.tearDownAction = tearDownAction;
                tearDownFunc = DoTearDown;
            }
            else
            {
                tearDownFunc = c => true;
            }

            this.isValidation = isValidation;
        }

        public string Key
        {
            get { return key; }
        }

        public bool IsValidation
        {
            get { return isValidation; }
        }

        public bool Execute(RunSequenceExecutionContext<T> runSequenceExecutionContext)
        {
            return executeFunc(runSequenceExecutionContext);
        }

        public bool TearDown(RunSequenceExecutionContext<T> runSequenceExecutionContext)
        {
            return tearDownFunc(runSequenceExecutionContext);
        }

        #region private

        bool DoExecuteAction(RunSequenceExecutionContext<T> runSequenceExecutionContext)
        {
            var userContext = runSequenceExecutionContext.UserContext;

            var runExecutionContext = new RunExecutionContext<T>
            {
                RunSequenceExecutionContext = runSequenceExecutionContext
            };

            try
            {
                action(userContext);
                return true;
            }
            catch (Exception exception)
            {
                //todo: log exception
                return false;
            }
        }

        bool DoExecuteAdvancedAction(RunSequenceExecutionContext<T> runSequenceExecutionContext)
        {
            var userContext = runSequenceExecutionContext.UserContext;

            var runExecutionContext = new RunExecutionContext<T>
            {
                RunSequenceExecutionContext = runSequenceExecutionContext
            };

            try
            {
                advancedAction(userContext, runExecutionContext);
                if (runExecutionContext.AsyncEvent != null)
                {
                    //todo: handle exception in another thread
                    if (!runExecutionContext.AsyncEvent.WaitOne(runExecutionContext.Timeout))
                    {
                        //todo: log timeout
                        return false;
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                //todo: log exception
                return false;
            }
        }

        bool DoTearDown(RunSequenceExecutionContext<T> runSequenceExecutionContext)
        {
            try
            {
                tearDownAction(runSequenceExecutionContext.UserContext);
                return true;
            }
            catch (Exception exception)
            {
                //todo: log exception
                return false;
            }
        }

        #endregion
    }
}