namespace FullFledgedTdd.TestDefinition
{
    public class RunSequenceExecutionContext<T>
    {
        public TestExecutionContext<T> TestExecutionContext { get; set; }
        public T UserContext { get; set; }
    }
}