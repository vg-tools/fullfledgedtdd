using System;
using FullFledgedTdd.ActionDefinition;

namespace FullFledgedTdd.TestDefinition
{
    public delegate ITestSet<T> TestSetFactory<T>(
        string testSetName,
        Func<T> contextCreator = null,
        Action<T> contextDisposer = null
    );

    /// <summary>
    /// Interface is exposed to client. Client puts test set definition in this class.
    /// </summary>
    public interface ITestSet<T>
    {
        void Add(ActionSet<T> actionSet);
        void Add(string testName, ActionSet<T> actionSet);

        //Execution is done in Add method.
        //Uncomment after integration with nUnit
        //void Execute(object nUnitIntegrationPoint);
    }
}