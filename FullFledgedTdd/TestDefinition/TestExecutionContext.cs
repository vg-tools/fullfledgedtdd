namespace FullFledgedTdd.TestDefinition
{
    public class TestExecutionContext<T>
    {
        public string TestName { get; set; }
        public TestSetExecutionContext<T> TestSetExecutionContext { get; set; }
    }
}