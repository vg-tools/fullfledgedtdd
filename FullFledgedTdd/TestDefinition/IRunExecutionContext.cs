namespace FullFledgedTdd.TestDefinition
{
    public interface IRunExecutionContext<T>
    {
        void SetAsync();
        void AsyncCallback();
    }
}