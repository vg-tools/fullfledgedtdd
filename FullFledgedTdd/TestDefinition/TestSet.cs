using System;
using System.Linq;
using System.Collections.Generic;
using FullFledgedTdd.ActionDefinition;

namespace FullFledgedTdd.TestDefinition
{
    public class TestSet<T> : ITestSet<T>
    {
        #region Ctor & fields

        private readonly string testSetName;
        private readonly Func<T> contextCreator;
        private readonly Action<T> contextDisposer;

        private readonly List<Tuple<string, ActionSetInternal<T>>> actionSets = new List<Tuple<string, ActionSetInternal<T>>>();

        public TestSet(
            string testSetName,
            Func<T> contextCreator = null,
            Action<T> contextDisposer = null)
        {
            this.testSetName = testSetName;
            this.contextCreator = contextCreator ?? Activator.CreateInstance<T>;
            this.contextDisposer = contextDisposer ?? DefaultContextDisposer;
        }

        #endregion

        public void Add(ActionSet<T> actionSet)
        {
            Add(null, actionSet);
        }

        public void Add(string testName, ActionSet<T> actionSet)
        {
            actionSets.Add(new Tuple<string, ActionSetInternal<T>>(testName, (ActionSetInternal<T>)actionSet));
            Execute(new object());
        }

        void Execute(object nUnitIntegrationPoint)
        {
            var testSetExecutionContext = new TestSetExecutionContext<T>
                                              {
                                                  TestSetName = testSetName,
                                                  NUnitIntegrationPoint = nUnitIntegrationPoint
                                              };

            foreach (var tuple in actionSets)
            {
                var testName = tuple.Item1;
                var actionSet = tuple.Item2;

                var testExecutionContext = new TestExecutionContext<T>
                {
                    TestName = testName,
                    TestSetExecutionContext = testSetExecutionContext
                };

                var runMatrix = GetRunMatrix(actionSet);
                foreach (var runSequence in runMatrix)
                {
                    ExecuteSequence(runSequence, testExecutionContext);
                }
            }
        }

        private void DefaultContextDisposer(T c)
        {
            var disposable = c as IDisposable;
            if (disposable != null) disposable.Dispose();
        }

        private IEnumerable<IEnumerable<IRun<T>>> GetRunMatrix(ActionSetInternal<T> actionSet)
        {
            //todo: trim after validation, remove wo validation
            var res = new List<IEnumerable<IRun<T>>>();

            foreach (var l in actionSet.GetRuns())
            {
                var raw = l.ToList();

                var indexOfValidationRun = raw.FindIndex(r => r.IsValidation);
                if (indexOfValidationRun == -1)
                {
                    continue;
                }
                var sequence = raw.Take(indexOfValidationRun + 1).ToList();
                if (!Contains(res, sequence))
                {
                    res.Add(sequence);
                }
            }

            return res;
        }

        private bool Contains(List<IEnumerable<IRun<T>>> listOfSequences, List<IRun<T>> targetSequence)
        {
            foreach (var aSequence in listOfSequences)
            {
                var b = aSequence.ToList();
                if (b.Count == targetSequence.Count)
                {
                    var equal = true;
                    for (int i = 0; equal && i < targetSequence.Count; i++)
                    {
                        equal &= targetSequence[i].Key == b[i].Key;
                    }
                    if (equal) return true;
                }
            }
            return false;
        }

        private void ExecuteSequence(
            IEnumerable<IRun<T>> runSequence,
            TestExecutionContext<T> testExecutionContext)
        {
            var runs = runSequence.ToList();

            var userContext = contextCreator();

            var runSequenceExecutionContext = new RunSequenceExecutionContext<T>
            {
                TestExecutionContext = testExecutionContext,
                UserContext = userContext
            };

            var index = 0;

            //run setup
            while (index < runs.Count - 1)
            {
                if (!runs[index++].Execute(runSequenceExecutionContext))
                {
                    return;
                }
            }

            //run validation
            //todo: log setup details both for validation success and fails
            if (runs[index].Execute(runSequenceExecutionContext))
            {
            }
            else
            {

            }

            //run teardown
            while (index > 0)
            {
                if (!runs[--index].TearDown(runSequenceExecutionContext))
                    return;
            }

            contextDisposer(userContext);
        }
    }
}