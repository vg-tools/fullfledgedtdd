namespace FullFledgedTdd.TestDefinition
{
    public class TestSetExecutionContext<T>
    {
        public string TestSetName { get; set; }
        public object NUnitIntegrationPoint { get; set; }
    }
}