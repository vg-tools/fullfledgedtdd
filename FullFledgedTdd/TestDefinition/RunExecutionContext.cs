using System;
using System.Threading;

namespace FullFledgedTdd.TestDefinition
{
    public class RunExecutionContext<T> : IRunExecutionContext<T>
    {
        public RunSequenceExecutionContext<T> RunSequenceExecutionContext { get; set; }
        
        #region Asyn action

        readonly TimeSpan defaultTimeout = new TimeSpan(1000);

        public ManualResetEvent AsyncEvent { get; private set; }
        public TimeSpan Timeout { get; private set; }

        public void SetAsync(TimeSpan timeout)
        {
            if (AsyncEvent != null)
            {
                throw new InvalidOperationException("SetAsync method called twice.");
            }

            AsyncEvent = new ManualResetEvent(false);
            //todo: provide ability to pass exception from async being tested method into test engine.
            Timeout = timeout;
        }

        public void SetAsync()
        {
            SetAsync(defaultTimeout);
        }

        public void AsyncCallback()
        {
            AsyncEvent.Set();
        } 

        #endregion
    }
}