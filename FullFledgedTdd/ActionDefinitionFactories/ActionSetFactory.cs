using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using FullFledgedTdd.ActionDefinition;
using Autofac;

namespace FullFledgedTdd.ActionDefinitionFactories
{
    public class ActionSetFactory<T>
    {
        private readonly UnionActionSetFactory<T> unionFactory;
        private readonly LeafActionSetFactory<T> leafActionSetFactory;
        private readonly LeafActionSetFactoryWithTearDown<T> leafActionSetFactoryWithTearDown;

        public ActionSetFactory()
        {
            unionFactory = FullFledgedTddModule.ComponentContext.Resolve<UnionActionSetFactory<T>>();
            leafActionSetFactory = FullFledgedTddModule.ComponentContext.Resolve<LeafActionSetFactory<T>>();
            leafActionSetFactoryWithTearDown = FullFledgedTddModule.ComponentContext.Resolve<LeafActionSetFactoryWithTearDown<T>>();
        }

        #region Create

        public ActionSet<T> Create(Action<T> action)
        {
            return leafActionSetFactory(action);
        }

        public ActionSet<T> Create(Action<T> action, Action<T> tearDown)
        {
            return leafActionSetFactoryWithTearDown(action, tearDown);
        }

        #endregion

        #region Set property

        public ActionSet<T> SetProperty<TProp>(IEnumerable<TProp> values, Expression<Func<T, TProp>> expression)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var memberName = memberExpression.Member.Name;
            var t = typeof(T);

            Action<T, TProp> setter;
            var pi = t.GetProperty(memberName);
            if (pi != null)
            {
                setter = (c, v) => pi.SetValue(c, v, null);
            }
            else
            {
                var fi = t.GetField(memberName);
                if (fi != null)
                {
                    setter = (c, v) => fi.SetValue(c, v);
                }
                else
                {
                    throw new ArgumentException(string.Format("Can't find member {0} at {1} calss.", memberName, t.Name));
                }
            }

            var actions = values.Select(v => new Action<T>(c => setter(c, v)));
            var actionSets = actions.Select(ActionSetExtensions.Create).ToList();

            return unionFactory(actionSets);
        }

        public ActionSet<T> AnyInt(Expression<Func<T, int>> predicate)
        {
            return SetProperty(
                new[] { 0, 1, -1, 100, -100 },
                predicate
            );
        }

        public ActionSet<T> AnyCount(Expression<Func<T, int>> predicate)
        {
            return SetProperty(
                new[] { 0, 1, 2, 5, 10, 100 },
                predicate
            );
        }

        public ActionSet<T> AnyText(Expression<Func<T, string>> predicate)
        {
            return SetProperty(
                new[] { "aaa", "bbb" },
                predicate
            );
        }

        #endregion
    }
}