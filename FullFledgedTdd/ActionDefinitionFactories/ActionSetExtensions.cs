using System;
using Autofac;
using FullFledgedTdd.ActionDefinition;

namespace FullFledgedTdd.ActionDefinitionFactories
{
    public static class ActionSetExtensions
    {
        #region Create

        public static ActionSet<T> Create<T>(Action<T> action)
        {
            return FullFledgedTddModule.ComponentContext.Resolve<LeafActionSetFactory<T>>()(action);
        }

        public static ActionSet<T> Create<T>(Action<T> action, Action<T> tearDown)
        {
            return FullFledgedTddModule.ComponentContext.Resolve<LeafActionSetFactoryWithTearDown<T>>()(action, tearDown);
        }

        #endregion

        #region To ActionSet

        public static ActionSet<T> ToActionSet<T>(this Action<T> source)
        {
            return FullFledgedTddModule.ComponentContext.Resolve<LeafActionSetFactory<T>>()(source);
        }

        //        public static ActionSet<T> ToActionSet<T>(this IEnumerable<Action<T>> source)
        //        {
        //            throw new NotImplementedException();
        //        }
        //
        //        public static ActionSet<T> ToActionSet<T>(this IEnumerable<ActionSet<T>> source)
        //        {
        //            throw new NotImplementedException();
        //        }

        #endregion

        #region Combine

        public static ActionSet<T> Combine<T>(this ActionSet<T> source, ActionSet<T> actionSet)
        {
            return FullFledgedTddModule.ComponentContext.Resolve<CombinationActionSetFactory<T>>()(new[] { source, actionSet });
        }

        public static ActionSet<T> Combine<T>(this ActionSet<T> source, Action<T> action)
        {
            var actionSet = FullFledgedTddModule.ComponentContext.Resolve<LeafActionSetFactory<T>>()(action);
            return FullFledgedTddModule.ComponentContext.Resolve<CombinationActionSetFactory<T>>()(new[] { source, actionSet });
        }

        #endregion

        #region Union

        public static ActionSet<T> Union<T>(this ActionSet<T> source, ActionSet<T> actionSet)
        {
            return FullFledgedTddModule.ComponentContext.Resolve<UnionActionSetFactory<T>>()(new[] { source, actionSet });
        }

        public static ActionSet<T> Union<T>(this ActionSet<T> source, Action<T> action)
        {
            var actionSet = FullFledgedTddModule.ComponentContext.Resolve<LeafActionSetFactory<T>>()(action);
            return FullFledgedTddModule.ComponentContext.Resolve<UnionActionSetFactory<T>>()(new[] { source, actionSet });
        }

        #endregion

        #region Validate

        public static ActionSet<T> Validate<T>(this ActionSet<T> source, Action<T> action)
        {
            return FullFledgedTddModule.ComponentContext.Resolve<ValidateActionSetFactory<T>>()(source, action/*, null*/);
        }

        /*public static ActionSet<T> Validate<T>(this ActionSet<T> source, Action<T> action, Action<T> tearDownAction)
        {
            return FullFledgedTddModule.ComponentContext.Resolve<ValidateActionSetFactory<T>>()(source, action, tearDownAction);
        }*/

        #endregion
    }
}
