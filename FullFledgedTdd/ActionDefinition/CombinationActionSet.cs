using System;
using System.Linq;
using System.Collections.Generic;
using FullFledgedTdd.TestDefinition;

namespace FullFledgedTdd.ActionDefinition
{
    public class CombinationActionSet<T> : ICombinationActionSet<T>
    {
        private readonly List<ActionSetInternal<T>> actionSets;

        public CombinationActionSet(IEnumerable<ActionSet<T>> actionSets)
        {
            this.actionSets = actionSets.OfType<ActionSetInternal<T>>().ToList();

            if (this.actionSets.Count < 2)
            {
                throw new ArgumentException("actionSets");
            }
        }

        public override IEnumerable<IRun<T>>[] GetRuns()
        {
            var combination = actionSets[0].GetRuns().ToArray();
            for (int i = 1; i < actionSets.Count; i++)
            {
                var runsToCombine = actionSets[i].GetRuns().ToList();

                combination = combination.SelectMany(l1 => runsToCombine.Select(l2 => l1.Union(l2))).ToArray();
            }
            return combination;
        }
    }
}