using System.Collections.Generic;

namespace FullFledgedTdd.ActionDefinition
{
    public delegate IUnionActionSet<T> UnionActionSetFactory<T>(IEnumerable<ActionSet<T>> actionSets);

    public abstract class IUnionActionSet<T> : ActionSetInternal<T>
    {

    }
}