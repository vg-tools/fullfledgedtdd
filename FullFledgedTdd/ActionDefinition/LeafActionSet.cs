using System;
using System.Collections.Generic;
using FullFledgedTdd.TestDefinition;

namespace FullFledgedTdd.ActionDefinition
{
    public class LeafActionSet<T> : ILeafActionSet<T>
    {
        private readonly Action<T> action;
        private readonly Action<T> tearDownAction;
        private readonly RunFactory<T> runFactory;

        public LeafActionSet(Action<T> action, RunFactory<T> runFactory)
            : this(action, null, runFactory)
        {
        }

        public LeafActionSet(Action<T> action, Action<T> tearDownAction, RunFactory<T> runFactory)
        {
            this.action = action;
            this.tearDownAction = tearDownAction;
            this.runFactory = runFactory;
        }

        public override IEnumerable<IRun<T>>[] GetRuns()
        {
            return new[] { new[] { runFactory(action, tearDownAction, false) } };
        }
    }
}
