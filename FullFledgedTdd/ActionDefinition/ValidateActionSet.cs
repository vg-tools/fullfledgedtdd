using System;
using System.Collections.Generic;
using System.Linq;
using FullFledgedTdd.TestDefinition;

namespace FullFledgedTdd.ActionDefinition
{
    public class ValidateActionSet<T> : IValidateActionSet<T>
    {
        private readonly ActionSet<T> actionSet;
        private readonly Action<T> action;
        /*private readonly Action<T> tearDownAction;*/
        private readonly RunFactory<T> runFactory;

        public ValidateActionSet(
            ActionSet<T> actionSet,
            Action<T> action,
            /*Action<T> tearDownAction,*/
            RunFactory<T> runFactory)
        {
            this.actionSet = actionSet;
            this.action = action;
            /*this.tearDownAction = tearDownAction;*/
            this.runFactory = runFactory;
        }

        public override IEnumerable<IRun<T>>[] GetRuns()
        {
            var runs = ((ActionSetInternal<T>)actionSet).GetRuns().ToList();
            var validationRun = runFactory(action, null, true);

            return runs.SelectMany(
                    run =>
                        new[]
                            {
                                run.Union(new[] { validationRun }),
                                run
                            }
                        )
                .ToArray();
        }
    }
}