using System.Collections.Generic;
using FullFledgedTdd.TestDefinition;

namespace FullFledgedTdd.ActionDefinition
{
    /// <summary>
    /// Used as interface. Exposes internal methods.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ActionSetInternal<T> : ActionSet<T>
    {
        //todo: return IRun<T>[][]
        public abstract IEnumerable<IRun<T>>[] GetRuns();
    }
}