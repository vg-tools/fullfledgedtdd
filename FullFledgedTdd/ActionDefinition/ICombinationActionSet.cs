using System.Collections.Generic;

namespace FullFledgedTdd.ActionDefinition
{
    public delegate ICombinationActionSet<T> CombinationActionSetFactory<T>(IEnumerable<ActionSet<T>> actionSets);
    
    public abstract class ICombinationActionSet<T> : ActionSetInternal<T>
    {

    }
}