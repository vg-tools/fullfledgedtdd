using System;

namespace FullFledgedTdd.ActionDefinition
{
    public delegate ILeafActionSet<T> LeafActionSetFactory<T>(Action<T> action);
    public delegate ILeafActionSet<T> LeafActionSetFactoryWithTearDown<T>(Action<T> action, Action<T> tearDownAction);

    public abstract class ILeafActionSet<T> : ActionSetInternal<T>
    {

    }
}