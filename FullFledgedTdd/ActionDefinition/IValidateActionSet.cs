using System;

namespace FullFledgedTdd.ActionDefinition
{
    public delegate IValidateActionSet<T> ValidateActionSetFactory<T>(
        ActionSet<T> actionSet, 
        Action<T> action/*,
        Action<T> tearDownAction*/);

    public abstract class IValidateActionSet<T> : ActionSetInternal<T>
    {

    }
}