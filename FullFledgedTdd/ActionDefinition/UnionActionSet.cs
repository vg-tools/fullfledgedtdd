using System.Linq;
using System.Collections.Generic;
using FullFledgedTdd.TestDefinition;

namespace FullFledgedTdd.ActionDefinition
{
    public class UnionActionSet<T> : IUnionActionSet<T>
    {
        private readonly IEnumerable<ActionSetInternal<T>> actionSets;

        public UnionActionSet(IEnumerable<ActionSet<T>> actionSets)
        {
            this.actionSets = actionSets.OfType<ActionSetInternal<T>>().ToList();
        }

        public override IEnumerable<IRun<T>>[] GetRuns()
        {
            return actionSets.SelectMany(s => s.GetRuns()).ToArray();
        }
    }
}