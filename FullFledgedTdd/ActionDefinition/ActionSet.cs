using FullFledgedTdd.ActionDefinitionFactories;

namespace FullFledgedTdd.ActionDefinition
{
    /// <summary>
    /// Action set definition.
    /// </summary>
    /// <remarks>
    /// Abstract class replaces interface. INterface can't be used because of operators overload.
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    public abstract class ActionSet<T>
    {
        public static ActionSet<T> operator &(ActionSet<T> a, ActionSet<T> b)
        {
            return a.Combine(b);
        }

        public static ActionSet<T> operator |(ActionSet<T> a, ActionSet<T> b)
        {
            return a.Union(b);
        }
    }
}