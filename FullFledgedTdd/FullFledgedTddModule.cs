﻿using Autofac;
using FullFledgedTdd.ActionDefinition;
using FullFledgedTdd.ActionDefinitionFactories;
using FullFledgedTdd.TestDefinition;

namespace FullFledgedTdd
{
    public class FullFledgedTddModule : Module
    {
        #region Static ComponentContext

        private static IComponentContext componentContext;

        public static IComponentContext ComponentContext
        {
            get { return componentContext ?? (componentContext = CreateDefaultComponentContext()); }
            set { componentContext = value; }
        }

        private static IComponentContext CreateDefaultComponentContext()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<FullFledgedTddModule>();
            return builder.Build();
        }

        #endregion

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(LeafActionSet<>)).As(typeof(ILeafActionSet<>));
            builder.RegisterGeneric(typeof(CombinationActionSet<>)).As(typeof(ICombinationActionSet<>));
            builder.RegisterGeneric(typeof(UnionActionSet<>)).As(typeof(IUnionActionSet<>));
            builder.RegisterGeneric(typeof(ValidateActionSet<>)).As(typeof(IValidateActionSet<>));

            builder.RegisterGeneric(typeof(Run<>)).As(typeof(IRun<>));
            builder.RegisterGeneric(typeof(TestSet<>)).As(typeof(ITestSet<>));

            builder.RegisterGeneric(typeof(ActionSetFactory<>));
        }
    }
}
