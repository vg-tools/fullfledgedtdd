using System.Reflection;
using NUnit.Core;

namespace FullFledgedTdd.NUnit
{
    public class DummyParameterizedMethodSuite : ParameterizedMethodSuite
    {
        public DummyParameterizedMethodSuite(MethodInfo method)
            : base(method)
        {
        }
        public override int CountTestCases(ITestFilter filter)
        {
            var countTestCases = base.CountTestCases(filter);
            return countTestCases;
        }
        public override TestResult Run(EventListener listener, ITestFilter filter)
        {
            var testResult = base.Run(listener, filter);
            return testResult;
            //            var testResult = new TestResult(this);
            //
            //            testResult.AddResult(new TestResult((Test)Tests[0]));
            //            testResult.AddResult(new TestResult((Test)Tests[1]));
            //
            //            testResult.Success();
            //            return testResult;
        }
    }
}