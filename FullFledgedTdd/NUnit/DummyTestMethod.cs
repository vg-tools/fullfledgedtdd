using System;
using System.Reflection;
using NUnit.Core;

namespace FullFledgedTdd.NUnit
{
    public class DummyTestMethod : TestMethod
    {
        public DummyTestMethod(MethodInfo method)
            : base(method)
        {
        }

        public override TestResult RunTest()
        {
            DateTime start = DateTime.Now;

            var testResult = new TestResult(this);
            TestExecutionContext.CurrentContext.CurrentResult = testResult;

            testResult.Success();

            DateTime stop = DateTime.Now;
            TimeSpan span = stop.Subtract(start);
            testResult.Time = (double)span.Ticks / (double)TimeSpan.TicksPerSecond;

            testResult.AssertCount = NUnitFramework.Assert.GetAssertCount();

            return testResult;
        }
    }
}