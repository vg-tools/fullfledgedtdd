using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using FullFledgedTdd.Set;
using NUnit.Core;
using NUnit.Core.Extensibility;

namespace FullFledgedTdd.NUnit
{
    public class SuiteBuilder : ISuiteBuilder
    {
        public Test BuildFrom(Type type)
        {
            if (!CanBuildFrom(type)) return null;

            var fixtureSuite = new NUnitTestFixture(type);
            foreach (var method in GetTargetMethods(type))
            {
                fixtureSuite.Add(BuildFromMethod(method));
            }
            return fixtureSuite;
        }

        private TestSuite BuildFromMethod(MethodInfo method)
        {
            var parameterizedMethodSuite = new DummyParameterizedMethodSuite(method);
            var prefix = parameterizedMethodSuite.TestName.FullName;

            var dummyTestMethod1 = new DummyTestMethod(method);
            dummyTestMethod1.TestName.Name = "parameterizedMethodSuite1";
            dummyTestMethod1.TestName.FullName = prefix + "(parameterizedMethodSuite1)";

            var dummyTestMethod2 = new DummyTestMethod(method);
            dummyTestMethod1.TestName.Name = "parameterizedMethodSuite2";
            dummyTestMethod1.TestName.FullName = prefix + "(parameterizedMethodSuite2)";

            parameterizedMethodSuite.Add(dummyTestMethod1);
            parameterizedMethodSuite.Add(dummyTestMethod2);

            return parameterizedMethodSuite;
        }

        #region Select target methods

        public bool CanBuildFrom(Type type)
        {
            return GetTargetMethods(type).Any();
        }

        private static IEnumerable<MethodInfo> GetTestableMethods(Type fixtureType)
        {
            return fixtureType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
        }

        private static IEnumerable<MethodInfo> GetTargetMethods(Type fixtureType)
        {
            return GetTestableMethods(fixtureType).Where(IsTargetMethod);
        }

        private static bool IsTargetMethod(MethodInfo method)
        {
            //            var parameters = method.GetParameters().ToArray();
            //
            //            if (parameters.Length != 1) return false;
            //
            //            var firstParameterType = parameters[0].ParameterType;
            //            if (
            //                !firstParameterType.IsGenericType ||
            //                !(firstParameterType.GetGenericTypeDefinition() == typeof(ITestSet<>))
            //            ) return false;

            return true;
        }

        #endregion
    }
}