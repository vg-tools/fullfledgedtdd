using System.Diagnostics;
using NUnit.Core.Extensibility;

namespace FullFledgedTdd.NUnit
{
    [NUnitAddin(Name = "FullFledgedTdd extension", Description = "Introduces advanced TDD practices.")]
    public class Addin : IAddin
    {
        public bool Install(IExtensionHost host)
        {
            Trace.WriteLine(string.Format("Addin {0} is being installed.", GetType().Name));

            return InstallSuiteBuilders(host);
        }

        private static bool InstallSuiteBuilders(IExtensionHost host)
        {
            var suiteBuilders = host.GetExtensionPoint("SuiteBuilders");
            if (suiteBuilders != null)
            {
                suiteBuilders.Install(new SuiteBuilder());
                return true;
            }

            return false;
        }
    }
}